#include "HX711.h"
#include <Servo.h>



#define calibration_factor 7050.0 //This value is obtained using the SparkFun_HX711_Calibration sketch

#define LOADCELL_DOUT_PIN  12
#define LOADCELL_SCK_PIN  3
Servo myservo;
HX711 scale;
int ledPin = 5;
float currTime;
float prevTime;
float prevDeriv = 0.0;
int pos = 5;
int finalPos = 120;
int initialPos = 5;

void brakeVoid() {
  //myservo.write(initialPos);
  analogWrite(ledPin, 0);
  /*if (pos != initialPos) {
    Serial.println("Braking");
    while(pos > initialPos) {
      myservo.write(pos);
      delay(8);
      pos -= 1;
    }
    //myservo.write(initialPos);
  }*/
}

void releaseVoid() {
  /*if (pos != finalPos) {
    Serial.println("Releasing");
    while(pos < finalPos) {
      myservo.write(pos);
      delay(8);
      pos += 1;
    }
    //myservo.write(finalPos);
  }*/
}


void setup() {
  pinMode(ledPin, OUTPUT);
  //myservo.attach(12);
  Serial.begin(115200);
  Serial.println("HX711 scale demo");

  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  scale.set_scale(); //This value is obtained by using the SparkFun_HX711_Calibration sketch
  scale.tare(); //Assuming there is no weight on the scale at start up, reset the scale to 0

  Serial.println("Readings:");
  prevTime = millis();
  /*releaseVoid();
  brakeVoid();
  for(int i = 3; i >= 1; i--) {
    Serial.println(i);
    delay(1000);
  }
  releaseVoid();*/
  delay(5000);


 
}


float setP = 5.0;
//int speed = 50;
float prevErr;
float thrState = 0.0;
float totIntegral = 0.0;
float secondIntegral = 0.0;
float thirdIntegral = 0.0;
float prevSecondDeriv = 0.0;
float prevIntegral = 0.0;
float prevSecondIntegral = 0.0;
float prevUnit = 0.0;
float prevPrevUnit = 0.0;
int cnt = 0;

void loop() {
  //Serial.print("Reading: ");
  scale.set_scale(calibration_factor);
  float unit = scale.get_units(1);
  if (cnt <= 30) {
    //scale.tare();
    unit = 0.0;
  }
  currTime = millis();
  unit = unit * 0.453592;
  unit = unit * 1.0;
  /*if(unit <= 0.0) {
    unit = 0.0;
  }*/
  float sendV = unit * 14.0;
  sendV = sendV + 50.0;
  if (sendV <= 0.0) {
    sendV = 0.0;
  } else if (sendV >= 210.0) {
    sendV = 210.0;
  }
  float timeEl = currTime - prevTime;
  float err = (unit - setP) * 1.0;
  if (err >= -0.4 && err <= 0.4) {
    err = 0.0;
  }
  float deriv = ((err - prevErr) / (currTime - prevTime)) * 200.0;
  totIntegral += (((err + prevErr) * 0.5) * (currTime - prevTime)) * 0.005;

 
  secondIntegral += (((totIntegral + prevIntegral) * 0.5) * (currTime - prevTime)) * 0.0005;
  thirdIntegral += (((secondIntegral + prevSecondIntegral) * 0.5) * (currTime - prevTime)) * 0.0001;
  if(totIntegral <= -50.0) {
    totIntegral = -50.0;
  } else if (totIntegral >= 50.0) {
    totIntegral = 50.0;
  }
  if(secondIntegral <= -50.0) {
    secondIntegral = -50.0;
  } else if (secondIntegral >= 50.0) {
    secondIntegral = 50.0;
  }
 

 
  if(thirdIntegral <= 50.0) {
    thirdIntegral = 50.0;
  } else if (thirdIntegral >= 180.0) {
    thirdIntegral = 180.0;
  }
  float sumIntegral = (totIntegral * 1.0) + (secondIntegral * 1.0) + (thirdIntegral * 2.0);
 
  float secondDeriv = ((deriv - prevDeriv) / (currTime - prevTime)) * 200.0;
  float thirdDeriv = ((secondDeriv - prevSecondDeriv) / (currTime - prevTime)) * 200.0;
  float pid = (err * 3.0) + (sumIntegral * 1.0) + (deriv * 7.0);
  if(pid <= 50.0) {
    pid = 50.0;
  } else if (pid >= 180.0) {
    pid = 180.0;
  }
  if(sumIntegral <= 50.0) {
    sumIntegral = 50.0;
  } else if (sumIntegral >= 180.0) {
    sumIntegral = 180.0;
  }
 
  int sendPid = (int)pid;
  float valD = unit * 17.0;
  int valSend = (int)valD;
  if (valSend >= 255) {
    valSend = 255;
  } else if (valSend <= 0) {
    valSend = 0;
  }
  //Serial.println(String(unit) + " " + String(err) + " " + String(totIntegral) +  +  " " + String(sumIntegral) + " " + String(sendPid));
  analogWrite(ledPin, sendPid);
  Serial.write(valSend);
  if(prevUnit <= 0.3 && unit <= 0.3 && prevPrevUnit <= 0.3) {
    brakeVoid();
  } else if (prevUnit > 0.3 && unit > 0.3 && prevPrevUnit > 0.3) {
    releaseVoid();
  }
  prevErr = err;
  prevDeriv = deriv;
  prevSecondDeriv = secondDeriv;
  prevIntegral = totIntegral;
  prevSecondIntegral = secondIntegral;
  prevTime = currTime;
  prevUnit = unit;
  prevPrevUnit = prevUnit;
  cnt += 1;
}
