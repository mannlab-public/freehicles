
import serial
import time
import csv

# Initialize serial connection
ser = serial.Serial('/dev/cu.usbserial-0001', 115200)  # Change '/dev/ttyUSB0' to your Arduino's serial port
time.sleep(2)  # Wait for Arduino to initialize

# Values to change based on range of values for each brain wave
num_led_strip = 72
final_num = 18
min_range_a = -0.09
max_range_a = 1.3
min_range_b = -0.3
max_range_b = 0.78
min_range_d = 0.03
max_range_d = 2.3
min_range_t = -0.0015 
max_range_t = 1.9


def mapped_value(wave, min_range, max_range, final_num):
    scaled_value = (wave - min_range) / (max_range - min_range)
    m_v = int(scaled_value * final_num)
    print (m_v)
    index = (m_v)*8
    return m_v, index

class FileTailer(object):
    def __init__(self, file, delay=0.1):
        self.file = file
        self.delay = delay
    def __iter__(self):
        while True:
            where = self.file.tell()
            line = self.file.readline()
            if line and line.endswith('\n'): # only emit full lines
                yield line
            else:                            # for a partial line, pause and back up
                time.sleep(self.delay)       # ...not actually a recommended approach.
                self.file.seek(where)
grat = [15, 30, 45, 60]
while True:
    csv_reader = csv.reader(FileTailer(open('test_plot.csv')))
    count = 0
    # for row in csv_reader:
        
    with open('test_plot.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        # Skip the header row if it exists
        next(csv_reader)
        # Iterate over each row in the CSV file
        count = 0
        for row in csv_reader:
    #         Extract the value from each column
            try:
                delta = float(row[0]) 
                theta = float(row[1])
                alpha = float(row[2])
                beta = float(row[3])
            except:
                delta = 0
                theta = 0
                alpha = 0
                beta = 0
                print("lmao")


            print("alpha:", alpha)
            print("beta:", beta)
            print("delta:", delta)
            print("theta:", theta)

            # Calculate the mapped value for each wave type
            mapped_alpha, index_alpha = mapped_value(alpha, min_range_a, max_range_a, final_num)
            mapped_beta, index_beta = mapped_value(beta, min_range_b, max_range_b, final_num)
            mapped_delta, index_delta = mapped_value(delta, min_range_d, max_range_d, final_num)
            mapped_theta, index_theta = mapped_value(theta, min_range_t, max_range_t, final_num)

            # 111 111 11
            # Create array  
            initial_value = "0" * (final_num * 8)


            modified_value_delta = initial_value[:index_delta] + "11100000" + initial_value [index_delta + 8:]
            modified_value_theta = initial_value[:index_theta] + "11111100" + initial_value[index_theta+ 8:]
            modified_value_alpha = initial_value[:index_alpha] + "00011100" + initial_value[index_alpha + 8:]
            modified_value_beta = initial_value[:index_beta] + "00000011" + initial_value[index_beta + 8:]

            led_strip = modified_value_delta + modified_value_theta + modified_value_alpha+ modified_value_beta

            packet = bytearray()

            for i in range(72):
                if(i in grat):
                    led_strip = led_strip[:i*8] + "00100001" + led_strip[i*8+8:]
                    # led_strip[i*8: i*8+8] = "00100001"
                

            # Adding all led_strip values
            for i in range(72):
                byte = led_strip[i*8: i*8+8]
                # print(byte  + " ", end = "")
                packet.append(int(byte, 2))

            while ser.in_waiting:  # Or: while ser.inWaiting():
                print("incoming: " + str(ser.readline()))

            ser.write(bytes('<', 'utf-8')) 
            ser.write(packet)
            ser.write(bytes('>', 'utf-8'))
            print()
            count += 1
            time.sleep(0.01)
            print("count: " + str(count))
            


# import serial
# import time
# import random
# import csv

# # Initialize serial connection
# ser = serial.Serial('/dev/cu.usbmodem101', 115200)  # Change '/dev/ttyUSB0' to your Arduino's serial port
# time.sleep(2)  # Wait for Arduino to initialize

# # Generate a random input value between -0.3 and 2.3
# min_range = -0.3
# max_range = 2.3
# #delta, theta, alpha, beta
# min_range = [0.03, -0.0015, -0.09, -0.3]
# max_range = [2.3, 1.9, 1.3, 0.78]

# num_led_strip = 72


# def convertToMappedIndex(row):
#     # delta = float(row[0]) 
#     # theta = float(row[1])
#     # alpha = float(row[2])
#     # beta = float(row[3])
    
#     excitedLEDIndex = []

#     for i in range(len(row)):
#         input_value = float(row[i])
        
#         #map from LED 0 to 71
#         mapped_value = int(( (input_value-min_range[i]) / (max_range[i]-min_range[i]) ) * (num_led_strip - 1) )
#         excitedLEDIndex.append(mapped_value)
#     return excitedLEDIndex
    

# def main():
#     while True:
#         with open('test_plot.csv', 'r') as csv_file:
#             csv_reader = csv.reader(csv_file)
#             # Skip the header row if it exists
#             next(csv_reader)
#             # Iterate over each row in the CSV file
#             for row in csv_reader:
#                 # Extract the value from the first column

                    

#                 # print("Input value:", input_value)

#                 # #first is the LED turned on for delta, then theta, alpha and beta
#                 excitedLedIndex = convertToMappedIndex(row)

                

#                 print(excitedLedIndex)
#                 # print("Mapped value:", scaled_value)

#                 # # Map scaled_value to a range from 0 to 72
#                 # mapped_value = int(scaled_value * num_led_strip)
#                 # print("Mapped value:", mapped_value)

#                 # # Create final_value with 576 zeroes
#                 # initial_value = "0" * (num_led_strip * 8)

#                 # # Calculate the index where the byte should change
#                 # index_to_change = (mapped_value-1) * 8




#                 # # Modify the byte at the calculated index to get it to the color red
#                 # modified_value = initial_value[:index_to_change] + "11100000" + initial_value[index_to_change + 8:]
#                 # led_strip = "<" + modified_value + ">"
#                 # print("Modified value:", led_strip)

#                 # # Send the modified value over serial
#                 # ser.write(led_strip.encode())  # Encode string to bytes and send

# if __name__ == "__main__":
#     main()

# # Close serial connection
# ser.close()