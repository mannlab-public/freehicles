import numpy as np
from pylsl import StreamInlet, resolve_byprop, StreamInfo, StreamOutlet
import utils
import csv

class Band:
    Delta = 0
    Theta = 1
    Alpha = 2
    Beta = 3

BUFFER_LENGTH = 5
EPOCH_LENGTH = 1
OVERLAP_LENGTH = 0.8
SHIFT_LENGTH = EPOCH_LENGTH - OVERLAP_LENGTH
INDEX_CHANNEL = [0]

if __name__ == "__main__":
    print('Looking for an EEG stream...')
    streams = resolve_byprop('type', 'EEG', timeout=2)
    if len(streams) == 0:
        raise RuntimeError('Can\'t find EEG stream.')
    PPG_streams = resolve_byprop('type', 'PPG', timeout=2)
    if len(PPG_streams) == 0:
        raise RuntimeError('Can\'t find PPG stream.')
    acc_streams = resolve_byprop('type', 'ACC', timeout=2)
    if len(acc_streams) == 0:
        raise RuntimeError('Can\'t find ACC stream.')
    gyro_streams = resolve_byprop('type', 'GYRO', timeout=2)
    if len(gyro_streams) == 0:
        raise RuntimeError('Can\'t find GYRO stream.')

    # muses = list_muses()
    # streams = stream(muses[0]['address'], ppg_enabled=True, acc_enabled=True, gyro_enabled=True)


    print("Start acquiring data")
    inlet = StreamInlet(streams[0], max_chunklen=12)
    eeg_time_correction = inlet.time_correction()

    ppg_inlet = StreamInlet(PPG_streams[0], max_chunklen=12)
    ppg_time_correction = ppg_inlet.time_correction()

    acc_inlet = StreamInlet(acc_streams[0], max_chunklen=12)
    acc_time_correction = acc_inlet.time_correction()

    gyro_inlet = StreamInlet(gyro_streams[0], max_chunklen=12)
    gyro_time_correction = gyro_inlet.time_correction()

    info = inlet.info()
    description = info.desc()
    fs = int(info.nominal_srate())



    ppg_info = ppg_inlet.info()
    print(ppg_info.desc().append_child("channels"))

    # exit()
    # description = ppg_info.desc()
    # ppg_fs = int(ppg_info.nominal_srate())

    # implement this later
    # Set up PPG stream
    # ppg_info = StreamInfo('Muse', 'PPG', 1, 64, 'float32', 'Muse_PPG')
    # ppg_info.desc().append_child_value("manufacturer", "Muse")
    # ppg_channels = ppg_info.desc().append_child("channels")

    # ppg_channels.append_child("channel") \
    #     .append_child_value("label", 'PPG') \
    #     .append_child_value("unit", "mmHg") \
    #     .append_child_value("type", "PPG")

    acc_info = acc_inlet.info()
    description = acc_info.desc()
    acc_fs = int(acc_info.nominal_srate())



    eeg_buffer = np.zeros((int(fs * BUFFER_LENGTH), 1))
    filter_state = None

    n_win_test = int(np.floor((BUFFER_LENGTH - EPOCH_LENGTH) /
                              SHIFT_LENGTH + 1))

    band_buffer = np.zeros((n_win_test, 4))

    csv_filename = "mete.csv"
    amplifier_factor = 1.5  # 

    with open(csv_filename, mode='w', newline='') as csv_file:
        # Create a CSV writer object
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow(['Timestamp', 'Delta', 'Theta', 'Alpha', 'Beta', 'PPG'])
        

        try:
            while True:
                eeg_data, timestamp = inlet.pull_chunk(
                    timeout=1, max_samples=int(SHIFT_LENGTH * fs))
                ch_data = np.array(eeg_data)[:, INDEX_CHANNEL]

                eeg_buffer, filter_state = utils.update_buffer(
                    eeg_buffer, ch_data, notch=True,
                    filter_state=filter_state)

                data_epoch = utils.get_last_data(eeg_buffer,
                                                 EPOCH_LENGTH * fs)

                band_powers = utils.compute_band_powers(data_epoch, fs)
                band_buffer, _ = utils.update_buffer(band_buffer,
                                                     np.asarray([band_powers]))
                smooth_band_powers = np.mean(band_buffer, axis=0)

                delta_metric = smooth_band_powers[Band.Delta] * amplifier_factor
                theta_metric = smooth_band_powers[Band.Theta] * amplifier_factor
                alpha_metric = smooth_band_powers[Band.Alpha] * amplifier_factor
                beta_metric = smooth_band_powers[Band.Beta] * amplifier_factor

                print('Delta (Amplified): ', delta_metric)
                print('Theta (Amplified): ', theta_metric)
                print('Alpha (Amplified): ', alpha_metric)
                print('Beta (Amplified): ', beta_metric)

                
                # ppg_data, _ = ppg_outlet.pull_sample()  # Assuming ppg_inlet is the PPG data stream inlet
                print(ppg_inlet.pull_sample())
                # print(ppg_inlet.samples_available())
                print(acc_inlet.pull_sample())
                print(gyro_inlet.pull_sample())

        except KeyboardInterrupt:
            print('Closing!')