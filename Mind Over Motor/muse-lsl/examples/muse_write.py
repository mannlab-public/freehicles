import numpy as np
from pylsl import StreamInlet, resolve_byprop
import utils
import csv

class Band:
    Delta = 0
    Theta = 1
    Alpha = 2
    Beta = 3

BUFFER_LENGTH = 5
EPOCH_LENGTH = 1
OVERLAP_LENGTH = 0.8
SHIFT_LENGTH = EPOCH_LENGTH - OVERLAP_LENGTH
INDEX_CHANNEL = [0]

if __name__ == "__main__":
    print('Looking for an EEG stream...')
    streams = resolve_byprop('type', 'EEG', timeout=2)
    if len(streams) == 0:
        raise RuntimeError('Can\'t find EEG stream.')

    print("Start acquiring data")
    inlet = StreamInlet(streams[0], max_chunklen=12)
    eeg_time_correction = inlet.time_correction()

    info = inlet.info()
    description = info.desc()
    fs = int(info.nominal_srate())

    eeg_buffer = np.zeros((int(fs * BUFFER_LENGTH), 1))
    filter_state = None

    n_win_test = int(np.floor((BUFFER_LENGTH - EPOCH_LENGTH) /
                              SHIFT_LENGTH + 1))

    band_buffer = np.zeros((n_win_test, 4))

    csv_filename = "mete.csv"

    amplifier_factor = 1.5  # 

    with open(csv_filename, mode='w', newline='') as csv_file:
        # Create a CSV writer object
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow(['Delta', 'Theta', 'Alpha', 'Beta'])

        try:
            while True:
                eeg_data, __ = inlet.pull_chunk(
                    timeout=1, max_samples=int(SHIFT_LENGTH * fs))
                ch_data = np.array(eeg_data)[:, INDEX_CHANNEL]

                eeg_buffer, filter_state = utils.update_buffer(
                    eeg_buffer, ch_data, notch=True,
                    filter_state=filter_state)

                data_epoch = utils.get_last_data(eeg_buffer,
                                                 EPOCH_LENGTH * fs)

                band_powers = utils.compute_band_powers(data_epoch, fs)
                band_buffer, _ = utils.update_buffer(band_buffer,
                                                     np.asarray([band_powers]))
                smooth_band_powers = np.mean(band_buffer, axis=0)

                delta_metric = smooth_band_powers[Band.Delta] * amplifier_factor
                theta_metric = smooth_band_powers[Band.Theta] * amplifier_factor
                alpha_metric = smooth_band_powers[Band.Alpha] * amplifier_factor
                beta_metric = smooth_band_powers[Band.Beta] * amplifier_factor

                print('Delta (Amplified): ', delta_metric)
                print('Theta (Amplified): ', theta_metric)
                print('Alpha (Amplified): ', alpha_metric)
                print('Beta (Amplified): ', beta_metric)

                csv_writer.writerow([delta_metric, theta_metric, alpha_metric, beta_metric])

        except KeyboardInterrupt:
            print('Closing!')