import numpy as np
from pylsl import StreamInlet, resolve_byprop
import utils
import csv
import time 
import serial

import pandas as pd
import matplotlib.pyplot as plt 
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import butter, filtfilt, find_peaks



##EMILY CODE
# Apply bandpass filter
def butter_bandpass(lowcut, highcut, _, order=2):
    nyquist = 0.5 * fs
    low = lowcut / nyquist
    high = highcut / nyquist
    b, a = butter(order, [low, high], btype='band', analog=False)
    return b, a

def bandpass_filter(data, lowcut, highcut, fs, order=2):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    filtered_data = filtfilt(b, a, data)
    return filtered_data

# Apply high-pass filter
def butter_highpass(cutoff, fs, order=2):
    nyquist = 0.5 * fs
    normal_cutoff = cutoff / nyquist
    b, a = butter(order, normal_cutoff, btype='high', analog=False)
    return b, a

def highpass_filter(data, cutoff_freq, fs, order=2):
    b, a = butter_highpass(cutoff_freq, fs, order=order)
    filtered_data = filtfilt(b, a, data)
    return filtered_data

# Define filter parameters
lowcut = 0.5  # Low cutoff frequency
highcut = 15  # High cutoff frequency
fs = 60  # Sample rate, adjust according to your data
order = 2  # Filter order, adjust as needed
### up to here


# Initialize serial connection
ser = serial.Serial('/dev/cu.usbmodem101', 115200)  # Change '/dev/ttyUSB0' to your Arduino's serial port
time.sleep(2)  # Wait for Arduino to initialize

# Values to change based on range of values for each brain wave
num_led_strip = 72
final_num = 72
min_range_a = 0 #0.2#-0.09
max_range_a = 1.5
min_range_b = -0.1 # 0# -0.3
max_range_b = 1.5
min_range_d = 0 #0.2 # 0.03
max_range_d = 2
min_range_t = 0 #0.2#-0.0015 
max_range_t = 1.5

class Band:
    Delta = 0
    Theta = 1
    Alpha = 2
    Beta = 3

BUFFER_LENGTH = 4
EPOCH_LENGTH = 1
OVERLAP_LENGTH = 0.8
SHIFT_LENGTH = EPOCH_LENGTH - OVERLAP_LENGTH
INDEX_CHANNEL = [0]


def mapped_value(wave, min_range, max_range, final_num):
    scaled_value = (wave - min_range) / (max_range - min_range)
    index = int(scaled_value * final_num)
    return index

if __name__ == "__main__":
    print('Looking for an EEG stream...')
    streams = resolve_byprop('type', 'EEG', timeout=2)
    if len(streams) == 0:
        raise RuntimeError('Can\'t find EEG stream.')
    
    PPG_streams = resolve_byprop('type', 'PPG', timeout=2)
    if len(PPG_streams) == 0:
        raise RuntimeError('Can\'t find PPG stream.')

    print("Start acquiring data")
    inlet = StreamInlet(streams[0], max_chunklen=12)
    eeg_time_correction = inlet.time_correction()

    ppg_inlet = StreamInlet(PPG_streams[0], max_chunklen=12)
    ppg_time_correction = ppg_inlet.time_correction()

    info = inlet.info()
    description = info.desc()
    # fs = int(info.nominal_srate())
    fs = 60

    ppg_info = ppg_inlet.info()
    print(ppg_info.desc().append_child("channels"))

    eeg_buffer = np.zeros((int(fs * BUFFER_LENGTH), 1))
    filter_state = None

    n_win_test = int(np.floor((BUFFER_LENGTH - EPOCH_LENGTH) /
                              SHIFT_LENGTH + 1))

    band_buffer = np.zeros((n_win_test, 4))

    csv_filename = "test.csv"

    amplifier_factor = 1.5  # 

    grat = [15, 30, 45, 60]

    ppg_window = []
    ppg_timestamp_window = []
    max_ppg_samples = 500

    

    with open(csv_filename, mode='w', newline='') as csv_file:
        # Create a CSV writer object
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow(['Delta', 'Theta', 'Alpha', 'Beta'])

        try:
            while True:
                
                eeg_data, __ = inlet.pull_chunk(
                    timeout=1, max_samples=int(SHIFT_LENGTH * fs))
                ch_data = np.array(eeg_data)[:, INDEX_CHANNEL]
                eeg_buffer, filter_state = utils.update_buffer(
                    eeg_buffer, ch_data, notch=True,
                    filter_state=filter_state)
                data_epoch = utils.get_last_data(eeg_buffer,
                                                 EPOCH_LENGTH * fs)
                band_powers = utils.compute_band_powers(data_epoch, fs)
                band_buffer, _ = utils.update_buffer(band_buffer,
                                                     np.asarray([band_powers]))
                smooth_band_powers = np.mean(band_buffer, axis=0)

                
                delta_metric = smooth_band_powers[Band.Delta] * amplifier_factor
                theta_metric = smooth_band_powers[Band.Theta] * amplifier_factor
                alpha_metric = smooth_band_powers[Band.Alpha] * amplifier_factor
                beta_metric = smooth_band_powers[Band.Beta] * amplifier_factor
                
                # print(smooth_band_powers)
                


                print('Delta (Amplified): ', delta_metric)
                print('Theta (Amplified): ', theta_metric)
                print('Alpha (Amplified): ', alpha_metric)
                print('Beta (Amplified): ', beta_metric)

                csv_writer.writerow([delta_metric, theta_metric, alpha_metric, beta_metric])


                print("alpha:", alpha_metric)
                print("beta:", beta_metric)
                print("delta:", delta_metric)
                print("theta:", theta_metric)

                # Calculate the mapped value for each wave type
                index_alpha = mapped_value(alpha_metric, min_range_a, max_range_a, final_num)
                print ("alpha index: " + str(index_alpha))
                index_beta = mapped_value(beta_metric, min_range_b, max_range_b, final_num)
                print ("beta index: " + str(index_beta))
                index_delta = mapped_value(delta_metric, min_range_d, max_range_d, final_num)
                print ("delta index: " + str(index_delta))
                index_theta = mapped_value(theta_metric, min_range_t, max_range_t, final_num)
                print ("theta index: " + str(index_theta))

                ppg_sample, ppg_timestamp = ppg_inlet.pull_sample()
                ppg_window.append(ppg_sample[0])
                ppg_timestamp_window.append(ppg_timestamp)
                if len(ppg_window) > max_ppg_samples:
                    ppg_window.pop(0)
                    ppg_timestamp_window.pop(0)
                
                window_samples = 15  # Equivalent to 0.25 seconds with the given sampling rate
                prominence_threshold = 1  # Adjust as needed
                # print("window: " + str(ppg_window))
                if(len(ppg_window) == max_ppg_samples):
                    ppg_filtered = bandpass_filter(ppg_window, lowcut, highcut, fs, order)
                                    # Find peaks
                    # print(ppg_filtered)
                    peaks, _ = find_peaks(ppg_filtered, height=100, distance=window_samples, prominence=prominence_threshold)
                    x = np.linspace(0, 8.6, 500)

                    timeDelta = ppg_timestamp_window[-1] - ppg_timestamp_window[0]
                    bpm = (len(peaks) / timeDelta) * 60
                    print(bpm)
                    # # Plot the data with peaks
                    # plt.plot(x, ppg_filtered, label='Filtered Data')
                    # plt.plot(x[peaks], ppg_filtered[peaks], "x", color='red', label='Peaks')
                    # plt.xlabel('X')
                    # plt.ylabel('Y')
                    # plt.title('Filtered Data with Peaks Detection (Within 0.1s Window)')
                    # plt.legend()
                    # plt.show()
                else:
                    ppg_filtered = [0]
                    peaks = []


                




                print(len(peaks))

                # 111 111 11
                # Create array  
                initial_value = "0" * (final_num * 8)

                if(index_alpha > 71):
                    print("exceeded alpha" + str(index_alpha))
                elif(index_alpha < 0):
                    print("negative alpha" + str(index_alpha))

                if(index_beta > 71):
                    print("exceeded beta" + str(index_beta))
                elif(index_beta < 0):
                    print("negative beta" + str(index_beta))
                
                if(index_delta > 71):
                    print("exceeded delta" + str(index_delta))
                elif(index_delta < 0):
                    print("negative delta" + str(index_delta))

                if(index_theta > 71):
                    print("exceeded theta" + str(index_theta))
                elif(index_theta < 0):
                    print("negative theta" + str(index_theta))

                led_strip = ""
                for i in range(72):
                    red = min(7, (index_delta == i) * 7 + (index_theta == i) * 7 + (i in grat) * 1)
                    green = min(7, (index_alpha == i) * 7 + (index_theta == i) * 7)
                    blue = min(3, (index_beta == i) * 3 + (i in grat) * 1)
                    
                    led_strip += format(red, '03b') + format(green, '03b') + format(blue, '02b')
                   
                packet = bytearray()
                # Adding all led_strip values
                for i in range(72):
                    byte = led_strip[i*8: i*8+8]
                    # print(byte  + " ", end = "")
                    packet.append(int(byte, 2))

                # while ser.in_waiting:  # Or: while ser.inWaiting():
                #     print("incoming: " + str(ser.readline()))

                ser.write(bytes('<', 'utf-8')) 
                ser.write(packet)
                ser.write(bytes('>', 'utf-8'))
                print()

        except KeyboardInterrupt:
            print('Closing!')
