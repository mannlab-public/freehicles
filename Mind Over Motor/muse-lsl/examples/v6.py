import numpy as np
from pylsl import StreamInlet, resolve_byprop
import utils

class EEGMetricsRecorder:
    def __init__(self):
        self.BUFFER_LENGTH = 5
        self.EPOCH_LENGTH = 1
        self.OVERLAP_LENGTH = 0.8
        self.SHIFT_LENGTH = self.EPOCH_LENGTH - self.OVERLAP_LENGTH
        self.INDEX_CHANNEL = [0]

        print('Looking for an EEG stream...')
        streams = resolve_byprop('type', 'EEG', timeout=2)
        if len(streams) == 0:
            raise RuntimeError('Can\'t find EEG stream.')

        print("Start acquiring data")
        self.inlet = StreamInlet(streams[0], max_chunklen=12)
        self.eeg_time_correction = self.inlet.time_correction()

        info = self.inlet.info()
        description = info.desc()
        self.fs = int(info.nominal_srate())

        self.eeg_buffer = np.zeros((int(self.fs * self.BUFFER_LENGTH), 1))
        self.filter_state = None

        n_win_test = int(np.floor((self.BUFFER_LENGTH - self.EPOCH_LENGTH) /
                                  self.SHIFT_LENGTH + 1))

        self.band_buffer = np.zeros((n_win_test, 4))

        # Use a list to store the alpha metrics dynamically
        self.alpha_metrics_list = []

        self.amplifier_factor = 1.5

    def run_recording(self):
        try:
            while True:
                eeg_data, timestamp = self.inlet.pull_chunk(
                    timeout=1, max_samples=int(self.SHIFT_LENGTH * self.fs))
                ch_data = np.array(eeg_data)[:, self.INDEX_CHANNEL]

                self.eeg_buffer, self.filter_state = utils.update_buffer(
                    self.eeg_buffer, ch_data, notch=True,
                    filter_state=self.filter_state)

                data_epoch = utils.get_last_data(self.eeg_buffer,
                                                 self.EPOCH_LENGTH * self.fs)

                band_powers = utils.compute_band_powers(data_epoch, self.fs)
                self.band_buffer, _ = utils.update_buffer(self.band_buffer,
                                                          np.asarray([band_powers]))
                smooth_band_powers = np.mean(self.band_buffer, axis=0)

                alpha_metric = smooth_band_powers[2] * self.amplifier_factor

                print('Alpha (Amplified): ', alpha_metric)

                # Append the alpha metric to the list
                self.alpha_metrics_list.append(alpha_metric)

                # Save alpha metric to a file
                with open("alpha_metrics.txt", "w") as alpha_file:
                    alpha_file.write(str(alpha_metric) + "\n")

        except KeyboardInterrupt:
            print('Closing!')

if __name__ == "__main__":
    recorder = EEGMetricsRecorder()
    recorder.run_recording()