// #include <Arduino.h>
// #include <esp_now.h>
// #include <WiFi.h>

// #include <Wire.h>
// // REPLACE WITH THE MAC Address of your receiver 
// uint8_t broadcastAddress[] = {0xb0,0xa7,0x32,0x2b,0x63,0x34};

// String success;

// typedef struct struct_message {
//   char[] character;
// } struct_message;

// const byte numBytes = 32; //change later with number of LEDs
// //figure out formatting for LED colour values
// byte receivedBytes[numBytes];
// byte numReceived = 0;

// boolean newData = false;

// void setup() {
//   Serial.begin(115200);
// }



// void recvBytesWithStartEndMarkers() {
//     static boolean recvInProgress = false;
//     static byte ndx = 0;
//     byte startMarker = 0x3C;
//     byte endMarker = 0x3E;
//     byte rb;
   

//     while (Serial.available() > 0 && newData == false) {
//         rb = Serial.read();

//         if (recvInProgress == true) {
//             if (rb != endMarker) {
//                 receivedBytes[ndx] = rb;
//                 ndx++;
//                 if (ndx >= numBytes) {
//                     ndx = numBytes - 1;
//                 }
//             }
//             else {
//                 receivedBytes[ndx] = '\0'; // terminate the string
//                 recvInProgress = false;
//                 numReceived = ndx;  // save the number for use when printing
//                 ndx = 0;
//                 newData = true;
//             }
//         }

//         else if (rb == startMarker) {
//             recvInProgress = true;
//         }
//     }
// }

// void showNewData() {
//     if (newData == true) {
//         Serial.print("This just in (HEX values)... ");
//         for (byte n = 0; n < numReceived; n++) {
//             Serial.print(receivedBytes[n], BIN);
//             Serial.print(' ');
//         }
//         Serial.println();
//         newData = false;
//     }
// }


// void loop() {
//   recvBytesWithStartEndMarkers();
//   showNewData();
// }


#include <esp_now.h>
#include <WiFi.h>

#include <Wire.h>
// REPLACE WITH THE MAC Address of your receiver b0:a7:32:2b:63:34
uint8_t broadcastAddress[] = {0xb0,0xa7,0x32,0x2b,0x63,0x34};
//84:fc:e6:6b:87:08 //this is of the goggle ESP
//34:85:18:ac:08:28 this is for the transmitter ESP

// Variable to store if sending data was successful
String success;

//Structure example to send data
//Must match the receiver structure
typedef struct struct_message {
    byte outgoingLED[72];
} struct_message;

// Create a struct_message called BME280Readings to hold sensor readings
// struct_message charToBeSent;

esp_now_peer_info_t peerInfo;
struct_message outgoingLED;

// Callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  // Serial.print("\r\nLast Packet Send Status:\t");
  // Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
  
  // Serial.println(charToBeSent.character);
  if (status ==0){
    success = "Delivery Success :)";
  }
  else{
    success = "Delivery Fail :(";
  }
}

const byte numBytes = 73;
byte receivedBytes[numBytes];
byte numReceived = 0;

boolean newData = false;

void setup() {
  Serial.begin(115200);

  pinMode(36, INPUT);
 
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }



  // Once ESPNow is successfully Init, we will regisuter for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  
  // Register peer
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.channel = 0;
  peerInfo.encrypt = false;
  
  // Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }
  // // Register for a callback function that will be called when data is received
  // esp_now_register_recv_cb(OnDataRecv);
}



void recvBytesWithStartEndMarkers() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    byte startMarker = 0x3C;
    byte endMarker = 0x3E;
    byte rb;
   

    while (Serial.available() > 0 && newData == false) {
        rb = Serial.read();

        if (recvInProgress == true) {
            if (rb != endMarker) {
                receivedBytes[ndx] = rb;
                ndx++;
                if (ndx >= numBytes) {
                    ndx = numBytes - 1;
                }
            }
            else {
                receivedBytes[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                numReceived = ndx;  // save the number for use when printing
                ndx = 0;
                newData = true;
            }
        }

        else if (rb == startMarker) {
            recvInProgress = true;
        }
    }
}

void showNewData() {
    // if (newData == true) {
    //    Serial.print("This just in (HEX values)... ");
    //     for (byte n = 0; n < numReceived; n++) {
    //         Serial.print(receivedBytes[n], BIN);
    //         Serial.print(' ');
    //     }
    //     Serial.println();
    //     memcpy(outgoingLED.outgoingLED, receivedBytes, sizeof(receivedBytes));
    
          
    //   esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &outgoingLED, sizeof(outgoingLED));
    //   if (result == ESP_OK) {
    //     Serial.println("Sent with success");
    //   }
    //   else {
    //     Serial.println("Error sending the data");
    //   }
    //   newData = false;
    // }

}
     
int startTime = millis();


void loop() {
  // recvBytesWithStartEndMarkers();
  // showNewData();

  int read = analogRead(36);
  Serial.println(read);
  int LEDIndex = (read / (4095.0 / 1.0)) * 71;
  Serial.println(LEDIndex);


  // int x = (millis() - startTime);
  // float y = sin(x);
  // int LEDIndex = int(35 * y) + 35; 
  // delay(50);
  
  for(int i = 0; i < 72; i++){
    if(i == LEDIndex)
      outgoingLED.outgoingLED[i] = 0x3C;
    else outgoingLED.outgoingLED[i] = 0x00;
    
  }

  

  esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &outgoingLED, sizeof(outgoingLED));
      // if (result == ESP_OK) {
      //   Serial.println("Sent with success");
      // }
      // else {
      //   Serial.println("Error sending the data");
      // }
  // Serial.println();
  
}


